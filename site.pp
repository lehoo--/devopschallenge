class { 'apache':
  mpm_module => 'prefork'
}

apache::vhost { 'localhost':
  port       => '80',
  docroot    => '/var/www/html',
}

include apache::mod::php

include '::mysql::server'

mysql::db { 'drupaldb':
  user     => 'ubuntu',
  host     => 'localhost',
  password => 'abc',
  grant    => ['SELECT', 'UPDATE', 'CREATE', 'DROP', 'INSERT', 'DELETE']
}

include ::php
include php::extension::mysql
include php::extension::gd
include php::extension::apc
