require 'net/http'
require './ssh_client.rb'

# one Amazon EC2 instance (t2.micro)
class Instance

  attr_reader :id
  attr_reader :public_dns_name

  def initialize(id, public_dns_name)
    @id = id
    @public_dns_name = public_dns_name
  end

  def setup_drupal
    install_puppet
    install_lamp
    install_drupal
  end

  def install_puppet
    SshClient.scp(@public_dns_name, ENV["PUPPET_CONF"])

    SshClient.connect_and_run_block(@public_dns_name) { |ssh|

      puts "**** installing puppet configuration management system ****"
      ssh.exec! "wget https://apt.puppetlabs.com/puppetlabs-release-trusty.deb"
      puts "**** wget puppet done ****"
      ssh.exec! "sudo dpkg -i puppetlabs-release-trusty.deb"
      puts "**** dpkg puppet done ****"
      ssh.exec! "sudo apt-get update"
      puts "**** apt-get update done ****"
      ssh.exec! "sudo apt-get install -y puppetmaster-passenger"
      puts "**** puppet installed ****"

      ssh.exec! "sudo apt-get update"
      ssh.exec! "puppet resource package puppetmaster ensure=latest"
      puts "**** puppet updated to latest ****"
    }
  end

  def install_lamp
    SshClient.connect_and_run_block(@public_dns_name) { |ssh|

      puts "**** setting up LAMP stack ****"
      ssh.exec! "sudo puppet module install puppetlabs-apache"
      puts "**** installed apache using puppet ****"
      ssh.exec! "sudo puppet module install puppetlabs-mysql"
      puts "**** installed mysql using puppet ****"
      ssh.exec! "sudo puppet module install nodes/php"
      puts "**** installed php using puppet ****"
      puts "**** applying site.pp ... ****"
      ssh.exec! "sudo puppet apply ~/site.pp"
    }
  end

  def install_drupal
    SshClient.connect_and_run_block(@public_dns_name) { |ssh|

      puts "**** setting up drupal ... ****"
      # install drupal
      ssh.exec! "sudo apt-get install -y drush"
      puts "**** drush installed ****"
      ssh.exec! "drush dl drupal-7.x"
      puts "**** drupal downloaded ****"
      puts "**** setting up the site ... ****"
      ssh.exec! "sudo rsync -avz drupal-7.x-dev/ /var/www"
      ssh.exec! "cd /var/www;sudo rm html/index.html;drush -y site-install standard --account-name=#{ENV["MYSQL_USER"]} --account-pass=#{ENV["MYSQL_PW"]} --db-url=mysql://#{ENV["MYSQL_USER"]}:#{ENV["MYSQL_PW"]}@localhost/#{ENV["DB_NAME"]}"
      puts "**** drupal set up ****"
    }
  end

  def verify_drupal
    uri = URI::HTTP.build({:host => @public_dns_name})
    resp = Net::HTTP.get_response(uri)

    begin
      if (resp.code == '200')
        puts "Return code 200, OK."
        drupal_line = look_for_drupal_in_response(Net::HTTP.get(uri))
        if drupal_line.nil?
          puts "Site doesn't seem to be a Drupal site."
        else
          puts "Drupal line: #{drupal_line}"
        end
      else
        puts "Problem connecting to instance. Return code #{resp.code}: #{resp.msg}."
      end
    rescue Errno::ECONNREFUSED
      puts "Problem connecting to instance: connection refused."
    rescue Timeout::Error
      puts "Problem connecting to instance: timeout error."
    end
  end

  def look_for_drupal_in_response(resp_str)
    resp_str.each_line do |line|
      if line.include? "Drupal"
        return line
      end
    end
    return
  end

  def terminate()
    resp = @client.terminate_instances({
                                         dry_run: false,
                                         instance_ids: [@id]
                                       })

    resp.terminating_instances.each { |item|
      puts "terminating instance with id #{item.instance_id}"
    }
  end

end
