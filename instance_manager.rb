require 'aws-sdk-core'
require './instance.rb'

# class which holds multiple instances and can be requested for operations on them
class InstanceManager
  REGION_EU_CENTRAL = 'eu-central-1'
  IMAGE_ID_UBUNTU = "ami-accff2b1"
  T2_MICRO = "t2.micro"

  def initialize
    @instances = Hash.new
    @client = Aws::EC2::Client.new(
                                   region: REGION_EU_CENTRAL
                                   )
  end

  def new_inst
    begin
      resp = @client.run_instances({
                                   dry_run: false,
                                   image_id: IMAGE_ID_UBUNTU,
                                   min_count: 1,
                                   max_count: 1,
                                   key_name: ENV["KEY_NAME"],
                                   security_group_ids: [ENV["SECURITY_GROUP_ID"]],
                                   instance_type: T2_MICRO
                                 })
    rescue Aws::EC2::Errors::InstanceLimitExceeded
      puts "You have too many running instances. You have to terminate some before requesting new ones."
      return
    end

    wait_for_instances(resp)
  end

  # wait for the newly created instances to be ready
  def wait_for_instances(run_instances_resp)
    instance_ids = Array.new
    run_instances_resp.instances.each { |inst|
      instance_ids.push(inst.instance_id)
    }
    while !instance_ids.empty? do
      puts "Waiting for instances to be ready ..."
      # we sleep a little, so we don't ping the instances non-stop
      sleep(5)
      # if we didn't specify any instance ids, this request would return all the instances we own
      # the problem with this is that it might return recently terminated instances
      # instead, we want to make sure to only request information about instances that we created during this session
      resp = @client.describe_instances({
                                          dry_run: false,
                                          instance_ids: instance_ids
                                        })
      resp.reservations[0].instances.each{ |inst|
        if inst.state.name == "running"
          @instances[inst.instance_id] = Instance.new(inst.instance_id, inst.public_dns_name)
          instance_ids -= [inst.instance_id]

          puts "instance with id #{inst.instance_id} and public dns #{inst.public_dns_name} is in #{inst.state.name} state."
        end
      }
    end
  end

  def list_instances
    if (@instances.empty?)
      puts "No instances created yet in this session."
      return
    end

    resp = @client.describe_instances({
                                        dry_run: false,
                                        instance_ids: @instances.keys
                                      })

    puts "instances created within this session:"
    resp.reservations.each { |reservation|
      reservation.instances.each { |inst|
        puts "id: #{inst.instance_id} ; public dns name: #{inst.public_dns_name} ; state: #{inst.state.name}"
      }
    }
  end

  def setup_drupal(id)
    if (!@instances.has_key?(id))
      puts "Instance with id #{id} not found!"
      return
    end

    puts "******** Setting up drupal to instance with id #{id} ********"
    @instances[id].setup_drupal
  end

  def verify_drupal(id)
    if (!@instances.has_key?(id))
      puts "Instance with id #{id} not found!"
      return
    end

    puts "******** Verifying drupal is running on instance with id #{id} ********"
    @instances[id].verify_drupal
  end

  def terminate_instance(id)
    if (!@instances.has_key?(id))
      puts "Instance with id #{id} not found!"
      return
    end

    puts "******** Terminating instance with id #{id} ********"
    resp = @client.terminate_instances({
                                         dry_run: false,
                                         instance_ids: [id]
                                       })
  end

  def terminate_instances
    resp = @client.terminate_instances({
                                         dry_run: false,
                                         instance_ids: @instances.keys
                                       })
  end

  def run_ssh_cmd_on_channel(ssh, cmd)
    puts "**********running #{cmd}"
    ssh.open_channel do |ch|
      ch.exec cmd do |c, success|
        abort "could not execute sudo su" unless success
      end

      ch.on_data do |c, data|
        puts "got stdout: #{data}"
      end

      ch.on_extended_data do |c, type, data|
        puts "got stderr: #{data}"
      end

      ch.on_close do |c|
        puts "channel is closing after command #{cmd}"
      end

      ch.wait
    end
  end

end
