require './instance_manager.rb'

class Interpreter

  def initialize
    @inst_man = InstanceManager.new()

    #@commands = { "create",
    #              "list",
    #              "install_puppet",
    #              "install_lamp",
    #              "install_drupal",
    #              "verify_drupal"
    #}
  end

  def interpret_command(cmd, args)
    case cmd
    when "create"
      new_inst
    when "list"
      list_instances
    when "install_puppet"
      install_puppet(args)
    when "install_lamp"
      install_lamp(args)
    when "setup_drupal"
      setup_drupal(args)
    when "verify_drupal"
      verify_drupal(args)
    when "terminate"
      terminate_instance(args)
    when "help"
      help
    when "exit"
      exit
    else
      puts "Unknown command: #{cmd}"
      puts "Type \'help\' for a list of available commands."
    end
  end

  def new_inst
    puts "******** Creating new instance ********"
    @inst_man.new_inst
    puts "******** DONE ********"
  end

  def list_instances
    puts "******** Listing instances ********"
    @inst_man.list_instances
    puts "******** DONE ********"
  end

  def setup_drupal(args)
    if (args.empty?)
      puts "Usage: setup_drupal <instance_id>"
      return
    end

    @inst_man.setup_drupal(args[0])
    puts "******** DONE ********"
  end

  def verify_drupal(args)
    if (args.empty?)
      puts "Usage: verify_drupal <instance_id>"
      return
    end

    @inst_man.verify_drupal(args[0])
    puts "******** DONE ********"
  end

  def terminate_instance(args)
    if (args.empty?)
      puts "Usage: terminate <instance_id>"
      return
    end

    @inst_man.terminate_instance(args[0])
    puts "******** DONE ********"
  end

  def help
    puts "******** Available commands ********"
    puts "create             - Create an Amazon EC2 t2.micro instance."
    puts "list               - List the instances created during this session."
    puts "setup_drupal <id>  - Set up Drupal on instance with id <id>."
    puts "verify_drupal <id> - Verifiy that Drupal has been correctly set up on instance with id <id>."
    puts "terminate <id>     - Terminate instance with id <id>."
    puts "help               - Display this help message."
    puts "exit               - Exit the program."

    puts ""
    puts "******** Environment variables ********"
    puts "KEY_NAME           - Name of the key pair to use."
    puts "KEYFILE            - Name of the keyfile that can be used to log in with SSH to the instance. Should be the keyfile corresponding to KEY_NAME (configured on the Amazon webpage)."
    puts "KEYFILE_FULLPATH   - Full path to the keyfile."
    puts "SECURITY_GROUP_ID  - ID of the security group to use."
    puts "USER_NAME          - Name of the user to log in to the instance. For the Ubuntu image used here, it should be \'ubuntu\'."
    puts "PUPPET_CONF        - The Puppet configuration file to use."
    puts "MYSQL_USER         - MySQL user name to use on the instance."
    puts "MYSQL_PW           - Password of the MySQL user on the instance."
    puts "DB_NAME            - Name of the MySQL database."
    puts "Note: MySQL credentials need to be the same as defined in the Puppet config file!"
  end

end
