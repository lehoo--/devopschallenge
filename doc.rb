#!/usr/bin/ruby

require './interpreter.rb'

def print_and_flush(str)
  print str
  STDOUT.flush
end

if __FILE__ == $0
  puts "This is a basic command line tool written in Ruby to create Amazon EC2 instances and set up a basic Drupal configuration on them."
  puts "Type \'help\' for a list of available commands."
  interpreter = Interpreter.new
  line = ''
  while (line != 'exit') do

    print_and_flush("> ")
    line = STDIN.readline
    if (!line.strip.empty?)
      words = line.split(' ')
      cmd = words.delete_at(0)
      interpreter.interpret_command(cmd, words)
    end
  end
end
