require 'net/ssh'

# handles SSH connections to instances
class SshClient
  TIMEOUT = 10
  MAX_RETRIES = 8

  def self.scp(url, path)
    continue = false
    retries = 0

    while (!continue) do
      ret_val = system "scp -oStrictHostKeyChecking=no -i #{ENV["KEYFILE_FULLPATH"]} " +  path + ' ' + ENV["USER_NAME"] + '@' + url + ':~/.'
      # connection refused can happen right before we can connect
      if !ret_val
        puts "Connection failed."
        retries += 1
        if retries < MAX_RETRIES
          puts "We wait for 5 seconds, then try again ..."
          sleep(5)
        else
          puts "Too many retries. Connection failed."
          continue = true
        end
      else
        continue = true
      end
    end
  end

  def self.connect_and_run_block(url)
    continue = false
    retries = 0

    while (!continue) do
      begin
        Net::SSH.start(url, ENV["USER_NAME"], :keys => [ENV["KEYFILE"]], :timeout => TIMEOUT) do |ssh|
          puts "connected to instance"
          yield ssh
          continue = true
        end
      # Errno:ECONNREFUSED can happen right before we can connect
      rescue Timeout::Error, Errno::ECONNREFUSED
        puts "Connection failed."
        retries += 1
        if retries < MAX_RETRIES
          puts "We wait for 5 seconds, then try again ..."
          sleep(5)
        else
          puts "Too many retries. Connection failed."
          continue = true
        end
      end
    end
  end

end
